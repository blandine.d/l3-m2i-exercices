class Ville:
    def __init__(self, nom, nbHabitants=0):
        self.n = nom.upper()
        self.nbH = nbHabitants

    #region GET/SET

    def get_Nom(self):
        return self.n

    def set_Nom(self, new):
        self.n = new

    def get_NbHabitants(self):
        return self.nbH

    def set_NbHabitants(self, new):
        self.nbH = new

    #endregion

    def nbHabitantsConnu(self):
        if (self.get_NbHabitants() > 0):
            return True
        else:
            return False

    def __str__(self):
        return "Nom de la Ville : " + self.n + " / Nombre d'habitants : " + str(self.nbH)

    def categorie(self):
        if ((self.get_NbHabitants()>0) and (self.get_NbHabitants()<500000)):
            c="A"
        elif (self.get_NbHabitants()>= 500000):
            c="B"
        else :
            c="?"
        return str(c)


