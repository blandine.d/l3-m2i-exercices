from Ville import *

class Capitale(Ville):
    def __init__(self, nom, nom_pays, nbHabitants=0):
        super().__init__(nom, nbHabitants)
        self.n_p = nom_pays.upper()

    def get_N_Pays(self):
        return self.n_p

    def set_N_Pays(self, new):
        self.n_p = new

    def __str__(self):
        return Ville.__str__(self) + " / Capitale de : " + self.n_p

    def nbHabitantsConnu(self):
        return Ville.nbHabitantsConnu(self)

    def categorie(self):
        return "C"