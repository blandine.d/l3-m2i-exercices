from Ville import *
from Capitale import *

v1=Ville("toulouse")
v2=Ville("strasbourg",272975)

print(str(v1))
print(str(v2))
print("")

c1=Capitale("paris","france")
c2=Capitale("rome", "italie", 210000)

print(str(c1))
print(str(c2))
print("")

print("Catégorie de le ville de "+ v1.get_Nom() + " : " + v1.categorie())
print("Catégorie de le ville de "+ v2.get_Nom() + " : " + v2.categorie())
print("Catégorie de le ville de "+ c1.get_Nom() + " : " + c1.categorie())
print("Catégorie de le ville de "+ c2.get_Nom() + " : " + c2.categorie())
