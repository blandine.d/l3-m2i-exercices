def est_un_palindrome(m):
    return m == m[::-1]

mot = input("Entrez un mot : ")
m = mot
p = est_un_palindrome(m)

if p == True:
    print(mot, 'est un palindrome')
else:
    print(mot,"n'est pas un palindrome")