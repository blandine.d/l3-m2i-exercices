from tkinter import *
from tkinter import filedialog
from Generation import *


class Application(Frame):
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre,**kwargs)



        frame1=Frame(fenetre, borderwidth=2,bg="#B4CCB4")
        frame1.pack(padx=10, pady=10)

        f1=Frame(frame1, borderwidth=2, relief="groove")
        f1.pack(padx=10, pady=10,side="left")

        Label(f1, text="Login : ", font=("ariel",12)).pack(padx=5, pady=5, side="left")
        self.login = Entry(f1, width=20)
        self.login.pack(padx=5, pady=5,side="left")

        f2 = Frame(frame1, borderwidth=2, relief="groove")
        f2.pack(padx=10, pady=10, anchor="ne",side="left")

        Label(f2, text="Mot de passe crypté : ", font=("ariel",12)).pack(padx=5, pady=5, side="left")
        self.mdp = Entry(f2, width=30)
        self.mdp.pack(padx=5, pady=5, side="left")

        #-------------------------------------------------------------------------------

        frame2 = Frame(fenetre, borderwidth=2,bg="#B4CCB4")
        frame2.pack(padx=100, pady=10,fill=X)

        self.choix = IntVar()
        b1 = Radiobutton(frame2,text="Lettre + Nom + Année",font=("ariel",12),var=self.choix,value=1,bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")
        b2 = Radiobutton(frame2,text="Dérivé d'un animal",font=("ariel",12),var=self.choix,value=2,bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")
        b3 = Radiobutton(frame2,text="Classique",font=("ariel",12), var=self.choix,value=3,bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")
        b4 = Radiobutton(frame2,text="Nombres devant ou derrière le nom + MAJ possible",font=("ariel",12), var=self.choix,value=4,bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")
        b5 = Radiobutton(frame2,text="Voyelles = chiffres / Consonnes = MAJ",font=("ariel",12), var=self.choix,value=5,bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")
        b6 = Radiobutton(frame2, text="Animal à l'envers x 2", font=("ariel", 12), var=self.choix, value=6, bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")
        b7 = Radiobutton(frame2, text="Concaténation de 2 animaux", font=("ariel", 12), var=self.choix, value=7, bg="#B4CCB4",activebackground="#B4CCB4").pack(anchor="w")

        # -------------------------------------------------------------------------------

        frame3 = Frame(fenetre, borderwidth=2, bg="#B4CCB4")
        frame3.pack(padx=100, pady=10, fill=X)

        self.recherche = Button(frame3, text="Lancer la rechercher", font=("ariel",12),command=self.search).pack()

        # -------------------------------------------------------------------------------

        frame4 = Frame(fenetre, borderwidth=2, bg="#B4CCB4")
        frame4.pack(padx=100, pady=10, fill=X)

        Label(frame4, text="Mot de passe : ", font=("ariel", 12), bg="#B4CCB4").pack(side="left", padx=15, pady=5)
        self.resultat = Label(frame4, text="",font=("ariel", 12), bg="#B4CCB4")
        self.resultat.pack(side="left")




    def search(self):

        g = Generation(self.login.get(),self.mdp.get())

        a = self.choix.get()
        rep = 0


        if a == 1:
            rep = g.choix1()
        elif a == 2:
            rep = g.choix2()
        elif a == 3:
            rep = g.choix3()
        elif a == 4:
            rep = g.choix4()
        elif a == 5:
            rep = g.choix5()
        elif a == 6:
            rep = g.choix6()
        elif a == 7:
            rep = g.choix7()

        if rep == 0:
            self.resultat["text"] = "Décryptage impossible"
        else:
            self.resultat["text"] = rep


