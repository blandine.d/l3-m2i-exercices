def binaire(x,nb=0):
    if x==0:
        y="0"
    else:
        y=""
        while x!=0:
            y="01"[x&1]+y
            x=x>>1
    return y.zfill(nb)