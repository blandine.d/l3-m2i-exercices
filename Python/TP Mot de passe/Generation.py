import hashlib
from Binaire import *


class Generation():
    def __init__(self, login, mdp):
        self.login = login
        self.mdp = mdp


    def choix1(self):
        # majuscule + nom + année
        for i in range(65, 91):
            for j in range(1900, 2020):
                resultat = chr(i) + self.login + str(j)
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat
        # minuscule + nom + année
        for i in range(97, 123):
            for j in range(190, 2020):
                resultat = chr(i) + self.login + str(j)
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat
        return 0

    def choix2(self):

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")
        nb = 0
        mots = []
        while fichier.readline():
            nb += 1
        fichier.close()


        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")
        for i in range(0,nb):
            mots.append(str(fichier.readline()))
        fichier.close()


        for m in mots:
            m = m[:len(m)-1]
            for i in range(0, 9999):
                # nom + nombre
                resultat = m + str(i)
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat

                # nombre + nom
                resultat = str(i) + m
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat

                # nombre + nom en MAJ
                resultat = str(i) + m.capitalize()
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat

                # nom en MAJ + nombre
                resultat = m.capitalize() + str(i)
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat

        return 0

    def choix3(self):

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")
        nb = 0

        while fichier.readline():
            nb += 1
        fichier.close()

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")

        for i in range(0, nb):

            m = list(fichier.readline())
            taillem = len(m)

            for j in range(0, 2 ** taillem):
                Binaire = str(binaire(j, taillem))
                for k in range(0, taillem):

                    if Binaire[k] == "1":
                        m[k] = m[k].upper()
                    else:
                        m[k] = m[k].lower()

                resultat = "".join(m)
                test = hashlib.md5(resultat[0:taillem - 1].encode()).hexdigest()

                if self.mdp == test:
                    fichier.close()
                    return resultat
        fichier.close()

        m = self.login
        taillem = len(m)

        for j in range(0, 2 ** taillem):
            resultat = ""
            Binaire = str(binaire(j, taillem))
            for k in range(0, taillem):

                if Binaire[k] == "1":
                    resultat += m[k].upper()
                else:  # met en minuscule
                    resultat += m[k].lower()

            test = hashlib.md5(resultat.encode()).hexdigest()

            if self.mdp == test:
                return resultat

        return 0

    def choix4(self):

        m = self.login

        for i in range(0, 9999):

            # nom + nombre
            resultat = m + str(('{0:04}'.format(i)))  # login + nombre
            test = hashlib.md5(resultat.encode()).hexdigest()
            if test == self.mdp:
                return resultat

            resultat = str('{0:04}'.format(i)) + m.capitalize()  # nombre + login maj
            test = hashlib.md5(resultat.encode()).hexdigest()
            if test == self.mdp:
                return resultat

            resultat = str('{0:04}'.format(i)) + m  # nombre + login
            test = hashlib.md5(resultat.encode()).hexdigest()
            if test == self.mdp:
                return resultat

            resultat = m.capitalize() + str(('{0:04}'.format(i)))  # login maj + nombre
            test = hashlib.md5(resultat.encode()).hexdigest()
            if test == self.mdp:
                return resultat
        return 0

    def choix5(self):

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")
        nb = 0
        mots = []
        while fichier.readline():
            nb += 1
        fichier.close()

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")
        for i in range(0, nb):
            mots.append(str(fichier.readline()))
        fichier.close()

        for m in mots:
            m = m[:len(m) - 1]
            for i in range(0, 9):
                resultat = ""
                for l in m:
                    if (
                            l == 'a' or l == 'e' or l == 'i' or l == 'o' or l == 'u' or l == 'y' or l == 'A' or l == 'E' or l == 'I'
                            or l == 'O' or l == 'U' or l == 'Y' or l == 'é' or l == 'è'):
                        resultat += str(i)
                    else:
                        # else : maj
                        resultat += str(l).upper()
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    return resultat
        return 0

    def choix6(self):

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")
        nb = 0

        while fichier.readline():
            nb += 1
        fichier.close()

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")

        for i in range(0, nb):

            a = "".join(reversed(fichier.readline().rstrip()))
            for j in range(0, 250):

                resultat = a * 2
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    fichier.close()
                    return resultat
        fichier.close()

        return 0

    def choix7(self):

        fichier1 = open("dico_animaux.txt", "r", encoding="UTF-8")
        nb = 0

        while fichier1.readline():
            nb += 1
        fichier1.close()

        fichier = open("dico_animaux.txt", "r", encoding="UTF-8")

        for i in range(0, 250):
            fichier2 = open("dico_animaux.txt", "r", encoding="UTF-8")
            a1 = fichier.readline().rstrip()
            for j in range(0, 250):
                a2 = fichier2.readline().rstrip()
                resultat = a1 + a2
                test = hashlib.md5(resultat.encode()).hexdigest()
                if test == self.mdp:
                    fichier1.close()
                    fichier2.close()
                    return resultat
            fichier2.close()
        fichier.close()

        return 0