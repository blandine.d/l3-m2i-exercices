M="CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGC"
R="CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGG"
P="AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCC"
L="CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"

def adn1(sequence1):

    if sequence1.count('CATA')!=0:
        return sequence1.find('CATA')
    else :
        return "x"

def adn2(sequence2):
    if sequence2.count('ATGC')!=0:
        return sequence2.find('ATGC')
    else:
        return "x"

list_suspect = [M,R,P,L]

for i in list_suspect :
    if (adn1(i)!= "x" and adn2(i)!="x") and (adn2(i)>adn1(i)+4 or adn2(i)<adn1(i)+1):
        if i =="CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGC":
            print("Moutarde est coupable")
        elif i =="CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGG":
            print("Rose est coupable")
        elif i == "AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCC":
            print("Pervenche est coupable")
        elif i =="CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG":
            print("Leblanc est coupable")

