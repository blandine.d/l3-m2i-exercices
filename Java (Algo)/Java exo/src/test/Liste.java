package test;

public class Liste {

		Integer tete;
		Liste queue;
		
	public Liste() {
			this.tete = null;
			this.queue = null;
			}
			
	
	public Liste(Integer i, Liste q) {
		this.tete = i; 
		this.queue = q;
		
	}
	
	public String toString() {
		if (this.estVide())
			return "()";
		else 
			return "("+this.tete+","+this.queue.toString()+")";
	}
	
	public boolean estVide() {
		if(this.tete==null && this.queue==null)
			return true;
		else return false;
	}
	
	public int taille() {
		if (this.estVide())
			return 0;
		else 
			return 1+this.queue.taille();
	}
	
	public Liste add(Integer i) {
		 if (this.estVide()) {
	            return new Liste(i, new Liste());
	        }else {
	            return new Liste(this.tete, this.queue.add(i));
	        }
	} 
	
	public boolean exist(Integer i) {
		   boolean result = false;
        if (!this.estVide()) {
            if (this.tete == i || this.queue.exist(i)) {
                result = true;
            }
        }else {
            result =false;
        }
        return result;
	}
	
	public Liste add(Integer i, int position) {
		   if (!this.estVide())  {
            if (this.queue.taille() < position) {
                position = this.queue.taille();
                return new Liste(this.tete, this.queue.add(i, position));
            }else {
                return new Liste(this.tete, this.queue.add(i, position - 1));
            }
        }else {
            return this.add(i);
        }
	}
	
	
	public static void main(String[] args) {
		Liste l = new Liste();
		Liste l2 = new Liste(2,new Liste());
		Liste l3 = new Liste(3,new Liste(2,new Liste()));
		
		System.out.print(l3);
		System.out.print("\r\n");
		System.out.print("Taille de la liste : " +l3.taille());
		

	}
	
	 public Liste suppr(Integer i) { 
	        if (!this.estVide()) {
	            if(this.tete == i) {
	                return this.queue;
	            }
	            else {
	                this.queue = this.queue.suppr(i);
	            }
	            return this;
	        }else {
	            return this;
	        }
	    }

}
