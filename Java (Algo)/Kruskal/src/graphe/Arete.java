package graphe;

public class Arete implements Comparable< Arete > {
	
	char origine;
	char destination;
	int poids;
	
	public Arete(char o, char d, int p) {
		this.origine = o;
		this.destination = d;
		this.poids = p;
		
	}
	
	 // Getter
	  public char getOrigine() {
	    return origine;
	  }
	  
	  public char getDestination() {
		    return destination;
		  }
	  
	  public Integer getPoids() {
		    return poids;
		  }

	  // Setter
	  public void setOrigine(char newOrigine) {
	    this.origine = newOrigine;
	  }

	  public void setDestination(char newDestination) {
		    this.destination = newDestination;
		  }
	  
	  public void setPoids(Integer newPoids) {
		    this.poids = newPoids;
		  }
	  
	  
	  public String toString() {
		 return this.origine+" -> "+this.destination+" = "+this.poids;
	  }
	  
	  public int compareTo(Arete x) {
	        return this.getPoids().compareTo(x.getPoids());
	    }
	  

}
