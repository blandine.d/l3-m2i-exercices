package graphe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Principal {
	
	public static boolean recherche(char a, char b, List<Arete> li) {
		
		int i =0;
		
		if(a==b) {
			return true;
		}
		
		if(li.size()<2) {
			return false;
		}
		
		for(Arete x : li) {
			char xa = x.getOrigine();
			char xb = x.getDestination();
			if(a==xa && b==xb) {
				return true;
			}
			if(xa == a) {
				List<Arete> li2 = new ArrayList<Arete>(li);
				li2.remove(i);
				if(recherche(xb,b, li2)) {
					return true;
				}
			}
			if(xb == a) {
				List<Arete> li2 = new ArrayList<Arete>(li);
				li2.remove(i);
				if(recherche(xa,b, li2)) {
					return true;
				}
			}
			i++;
		}
		return false;
	}
	
	 public static void main(String[] args) {
		 
		 ArrayList<Arete> arete = new ArrayList<Arete>(); 
			arete.add(new Arete ('A','B',1));
			arete.add(new Arete ('A','C',4));
			arete.add(new Arete ('B','C',3));
			arete.add(new Arete ('C','F',18));
			arete.add(new Arete ('C','E',10));
			arete.add(new Arete ('B','E',9));
			arete.add(new Arete ('B','D',5));
			arete.add(new Arete ('A','D',4));
			arete.add(new Arete ('D','E',7));
			arete.add(new Arete ('D','G',9));
			arete.add(new Arete ('D','I',9));
			arete.add(new Arete ('E','F',8));
			arete.add(new Arete ('E','G',8));
			arete.add(new Arete ('E','H',9));
			arete.add(new Arete ('G','I',2));
			arete.add(new Arete ('G','H',2));
			arete.add(new Arete ('I','H',4));
			arete.add(new Arete ('I','J',6));
			arete.add(new Arete ('H','J',3));
			arete.add(new Arete ('H','F',9));
			arete.add(new Arete ('J','F',9));

			Collections.sort(arete);
			

			List<Arete> resultat = new ArrayList<Arete>();
			for(Arete a : arete) {
				if(recherche(a.getOrigine(), a.getDestination(), resultat)==false ){
					resultat.add(a);
				}
			}
			
			System.out.println("Les ar�tes de l'arbre couvrant minimum sont : \n");
			for(Arete a: resultat) {
				System.out.println(a.toString());
			}
		  }
	 
	 

}
