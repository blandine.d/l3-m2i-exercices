package Model;

import java.sql.Date;

class Commentaires extends Billet {
	
	private String comment, auteur, titre;
	private Date date;
	
	public Commentaires() {
		this.comment = "";
	}
	
	public void setComment(String com) {
		this.comment = com;
	}
	    
	public String getComment() {
	    return comment;
	}
	    
	public String getAuteur() {
	    return auteur;
	}
	    
	public void setAuteur(String auteur) {
	    this.auteur = auteur;
	}
	  
	public String getTitre() {
	    return titre;
	}

	public void setTitre(String titre) {
	    this.titre = titre;
	}
	    
	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
