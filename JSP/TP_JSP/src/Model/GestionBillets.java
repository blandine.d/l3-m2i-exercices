package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class GestionBillets {

    private List<Billet> billets;
    
    public GestionBillets() {
        this.billets = new ArrayList<>();
    }

    public void add(Billet billet) {
        this.billets.add(billet);
    }

    public Billet getBillet(int i) {
        return billets.get(i);
    }

    public Billet getLastBillet() {
        if (billets.size() > 0)
            return this.getBillet(billets.size() -1);
        throw new IndexOutOfBoundsException("Erreur dans l'appel à la fonction getLastBillet");
    }
    
    public static void main(String[] args) {
    	
	}
}
