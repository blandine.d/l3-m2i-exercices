package Model;

import java.util.Scanner;
import java.sql.*;

public class Billet {
    private String titre, contenu, auteur, statut, commentaire;
    private int id, idA, idS;
    private Date date;
    
    public Billet() {
        this.titre = "Rien";
        this.contenu = "Vide";
        this.auteur = "Personne";
        this.statut = "Rien";
    }
    
    public Billet(String titre, String contenu, int idA, int idS) {
        this.titre = titre;
        this.contenu = contenu;
        this.idA = idA;
        this.idS = idS;
    }
    
    public Billet(String titre, String contenu, String auteur) {
    	this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
    }
    
    public Billet(String comment, String auteur, Date datePub) {
        this.commentaire = comment;
        this.auteur = auteur;
        this.date = datePub;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }
    
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
    
    public String getStatut() {
        return statut;
    }
    
    public void setIdA(int id) {
        this.idA = id;
    }
    
    public int getIdA() {
        return idA;
    }
    
    public void setIdS(int id) {
        this.idS = id;
    }
    
    public int getIdS() {
        return idS;
    }
    
    public void setComment(String com) {
        this.commentaire = com;
    }
    
    public String getComment() {
        return commentaire;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public Date getDate() {
        return date;
    }
    
  
    public static void main(String[] args) {
    	Connect con = new Connect();
//    	Scanner scan = new Scanner(System.in);
//    	String titre = scan.nextLine();
//    	String content = scan.nextLine();
//    	String pseudo = scan.nextLine();
//    	int id = Integer.parseInt(pseudo);
//        Billet bil = new Billet(titre, content, pseudo);
//    	con.insertBillet(bil);
	}
    
}
