package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Connect {
	static Connection conn;
	static PreparedStatement statement;
	public Connect() {
		try { 
			String strClassName = "com.mysql.jdbc.Driver";
			String dbName= "jsp"; 
			String login= "root"; 
			String motdepasse= "root"; 
			String strUrl = "jdbc:mysql://localhost:8889/"+ dbName + "?useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC";
			
			Class.forName(strClassName);
			conn = DriverManager.getConnection(strUrl, login, motdepasse);	
			}
			catch(ClassNotFoundException e) {  
				System.err.println("Driver non chargÈ !");  e.printStackTrace();
			} catch(SQLException e) {
				System.err.println("Autre erreur !");  e.printStackTrace();
			}	
	}
	
	public boolean selectUser(String nom) {
		boolean trouve = false;
		try {
			Statement stSel = conn.createStatement();
			String pseudo = nom;
			String selQuery = "Select * from author where pseudo = '" + pseudo + "'";
			ResultSet rsUser = stSel.executeQuery(selQuery);
			if (rsUser.next() == false) {  //si le resultset = vide
				trouve = false;
			} else {
				trouve = true;
				String result = "L'utilisateur existe déjà!";
			} //while (rsUser.next());  //si resultset != vide - pour eviter de louper la first data
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return trouve;  
	}
	
	public int User(String nom) {
		int idA = 0;
		try {
			Statement stSel = conn.createStatement();
			String pseudo = nom;
			String selQuery = "Select * from author where pseudo = '" + pseudo + "'";	
			ResultSet rsUser = stSel.executeQuery(selQuery);
			while (rsUser.next()) {
				idA = rsUser.getInt("id");
			}			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idA;
	}
	
	public boolean selectGrpe(String nom) {
		boolean trouve = false;
		try {
			Statement stSel = conn.createStatement();
			String nomgrpe = nom;
			String selQuery = "Select * from groupe where nom = '" + nomgrpe + "'";
			ResultSet rsUser = stSel.executeQuery(selQuery);
			if (rsUser.next() == false) {  //si le resultset = vide
				trouve = false;
			} else {
				trouve = true;
				String result = "Le groupe existe déjà!";
			} 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return trouve;  
	}
	
	public int Billet(String nom) {
		int idB = 0;
		try {
			Statement stSel = conn.createStatement();
			String titre = nom;
			String selQuery = "Select * from billet where titre = '" + titre + "'";	
			ResultSet rsBillet = stSel.executeQuery(selQuery);
			while (rsBillet.next()) {
				idB = rsBillet.getInt("idb");
			}			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idB;
	}
	
	public List<Billet> selectBillet() {
		List<Billet> list = new ArrayList<Billet>();
		try {
			Statement stSel = conn.createStatement();
			String selQuery = "Select * from billet";	
			
			ResultSet rsUser = stSel.executeQuery(selQuery);
			while(rsUser.next()) {
				Billet b = new Billet(rsUser.getString("titre"), rsUser.getString("content"), rsUser.getString("pseudo"));
				list.add(b);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	public List<Billet> selectComment(int idB) {
		List<Billet> list = new ArrayList<Billet>();
		try {
			Statement stSel = conn.createStatement();
			String selQuery = "Select c.*, a.* from commentaire c, author a where c.idAu = a.id and c.idB = " + idB;	
			
			ResultSet rsComment = stSel.executeQuery(selQuery);
			while(rsComment.next()) {
				Billet b = new Billet(rsComment.getString("commentaire"), rsComment.getString("pseudo"), rsComment.getDate("Date"));
				list.add(b);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	public void insertUser(String nom) {
		try {
			Statement stAdd = conn.createStatement();
			String pseudo = nom;
			String addQuery = "Insert into author (pseudo) value ('" + pseudo + "')";
			stAdd.executeUpdate(addQuery);
			String result = "L'utilisateur a bien été créé!";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public void insertGrpe(String groupe) {
		try {
			Statement stAdd = conn.createStatement();
			String nomgrp = groupe;
			String addQuery = "Insert into groupe (nom) value ('" + nomgrp + "')";
			stAdd.executeUpdate(addQuery);
			String result = "Le groupe a bien été créé !";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public Billet insertBillet(Billet b) {
		try {
			Statement stAdd = conn.createStatement();
			Statement stSel = conn.createStatement();
			int idS = Integer.parseInt(b.getStatut());
			String addQuery = "Insert into billet (titre, content, idA, idS) value ('" + b.getTitre() + "','" + b.getContenu() + "', " + b.getIdA() + "," + idS + ")";
			stAdd.executeUpdate(addQuery);
			String result = "Le billet a bien été enregistré !";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b; 
	}
	
	public void insertComment(String content, int idB, int idA) {
		try {
			Statement stAdd = conn.createStatement();
			String comment = content;
			int idb = idB;
			int ida = idA;
			String addQuery = "Insert into commentaire (commentaire, idB, idAu) value ('" + comment + "'," + idb + "," + ida + ")";
			stAdd.executeUpdate(addQuery);
			String result = "Le commentaire a bien été enregistré !";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public void deleteBillet(int idB) {
		try {
			Statement stAdd = conn.createStatement();
			int idb = idB;
			String addQuery = "Delete from billet where idb = " + idB;
			stAdd.executeUpdate(addQuery);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Connect con = new Connect();
		con.insertUser("jelly");
	}

	
}
