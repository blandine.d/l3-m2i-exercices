package Controller;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import Model.Connect;


@WebServlet(name = "Init", urlPatterns = "/Init")
public class Init extends HttpServlet {    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    	//this.getServletContext().getRequestDispatcher("/WEB-INF/index.html").forward(request, response);
	    response.sendRedirect("index.html");
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pseudo = request.getParameter("pseudo");
        String groupe = request.getParameter("groupe");
        Connect con = new Connect();  
        HttpSession session = request.getSession(true);
        if(pseudo != null && !pseudo.equals("")) {  
        	if (con.selectUser(pseudo) == false) {
        		 con.insertUser(pseudo);
        		 session.setAttribute("pseudo", pseudo);
                 request.getRequestDispatcher("billets.jsp").forward(request, response);
        	} else {
        		session.setAttribute("pseudo", pseudo);    
                request.getRequestDispatcher("billets.jsp").forward(request, response);
        	}	
        } else if(groupe != null && !groupe.equals(null)) {
        	if (con.selectGrpe(groupe) == false) {
        		con.insertGrpe(groupe);
        		session.setAttribute("group", groupe);
            	request.getRequestDispatcher("billets.jsp").forward(request, response);
        	} else {
        		session.setAttribute("group", groupe);
            	request.getRequestDispatcher("billets.jsp").forward(request, response);
        	}	
        } else {
        	response.sendRedirect("index.html");
        } 
    }
}

