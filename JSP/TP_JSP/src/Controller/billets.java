package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Billet;
import Model.Connect;

/**
 * Servlet implementation class billets
 */
@WebServlet("/billets")
public class billets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public billets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String comment = request.getParameter("commentaire");
		//int idB = Integer.parseInt(request.getParameter("idB"));
		//int idA = Integer.parseInt(request.getParameter("idA"));
		
        HttpSession session = request.getSession(true);
        
        if(comment != null && !comment.equals("")) {  
            session.setAttribute("commentaire", comment);
            //Connect con = new Connect();
            //con.insertComment(comment, idB, idA);
            request.getRequestDispatcher("billet.jsp").forward(request, response);
        } else {
        	response.sendRedirect("billet.jsp");
        }	
      
        
	}

}
