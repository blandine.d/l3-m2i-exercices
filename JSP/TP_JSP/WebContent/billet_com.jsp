<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page language="java" import="java.util.*,java.lang.*" %> 
<%@ page import="Model.Billet" %>
<%@ page import="Model.GestionBillets" %>
<%@ page import="Model.Connect" %>
<%! private Billet billet = new Billet(); %>
<% if (request.getMethod().equals("POST")) {
    billet.setContenu(request.getParameter("contenu"));
    billet.setTitre(request.getParameter("titre"));
    billet.setAuteur((String) session.getAttribute("pseudo"));
    billet.setStatut(request.getParameter("statut"));
    billet.setComment(request.getParameter("commentaire"));
} %>
<!doctype html>
<html>
<head>
    <title>Billet</title>
</head>
<body>
<%-- 	<p><%= idA %></p> --%>
<%-- 	<p><%= idB %></p> --%>
<%
	Connect con = new Connect();
	String pseudo = (String)session.getAttribute("pseudo");
	String titre = request.getParameter("titre");
	int idA = con.User(pseudo);
	int idB = con.Billet(titre);
	if (billet.getComment() != null && !billet.getComment().equals(null))
	{
		String comment = billet.getComment();
		con.insertComment(comment, idB, idA);
		
	}
%>
	<form method="post" action="Billet.co">
		<p>Pseudo : <input type="text" name="pseudo" value="<%= pseudo %>" readonly></p>
		<p>Billet : <input type="text" name="titre" value="<%= titre %>" readonly></p>
		<hr>
   	 	<p>
        Commentaire :
        <input type="text" name="commentaire" value="">
        <input type="submit" value="Envoyer">
    	</p>
	</form>
	<hr>
	<%
		List<Billet> liste = con.selectComment(idB);
		Iterator<Billet> la_liste = liste.iterator();
	%>
	<table border="1">
	<% 
		while(la_liste.hasNext()){
			Billet b = new Billet();
			b = la_liste.next();
	%>
			<tr>
			<td><%= b.getComment() %></td>
			<td><%= b.getAuteur() %></td>
			<td><%= b.getDate() %></td>
			<td><a href="Commentaire?titre=">Supprimer</a></td>
		    </tr>
	<%
		}
	%>
	</table>
	<hr>
	
<p><a href="saisie.html">Saisir un nouveau billet</a></p>
<p><a href="Deco">Se déconnecter</a></p>
</body>
</html>